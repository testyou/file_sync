#include "app.h"
#include "file.h"
#include "config.h"
#include "daemon.h"
#include "strings.h"
#include <stdio.h>
#include <dirent.h>

#include "threadpool.h"
#include "inotify.h"

#include "log.h"

int Application::main(int argc, char **argv){
	conf = NULL;

	welcome();
	parse_args(argc, argv);        
	init();
        
	write_pid();
	run();
	remove_pidfile();
	
	delete conf;
	return 0;
}

void Application::usage(int argc, char **argv){
	printf("Usage:\n");
	printf("    %s [-d] /path/to/app.conf [-s start|stop|restart]\n", argv[0]);
	printf("Options:\n");
	printf("    -d    run as daemon\n");
	printf("    -s    option to start|stop|restart the server\n");
	printf("    -h    show this message\n");
}

void Application::parse_args(int argc, char **argv){
	for(int i=1; i<argc; i++){
		std::string arg = argv[i];
		if(arg == "-d"){
			app_args.is_daemon = true;
		}else if(arg == "-v"){
			exit(0);
		}else if(arg == "-h"){
			usage(argc, argv);
			exit(0);
		}else if(arg == "-s"){
			if(argc > i + 1){
				i ++;
				app_args.start_opt = argv[i];
			}else{
				usage(argc, argv);
				exit(1);
			}
			if(app_args.start_opt != "start" && app_args.start_opt != "stop" && app_args.start_opt != "restart"){
				usage(argc, argv);
				fprintf(stderr, "Error: bad argument: '%s'\n", app_args.start_opt.c_str());
				exit(1);
			}
		}else{
			app_args.conf_file = argv[i];
		}
	}

	if(app_args.conf_file.empty()){
		usage(argc, argv);
		exit(1);
	}
}

void Application::init(){
	if(!is_file(app_args.conf_file.c_str())){
		fprintf(stderr, "'%s' is not a file or not exists!\n", app_args.conf_file.c_str());
		exit(1);
	}
        conf = Config::load(app_args.conf_file.c_str());	
        
	if(!conf){
		fprintf(stderr, "error loading conf file: '%s'\n", app_args.conf_file.c_str());
		exit(1);
	}
	{
		std::string conf_dir = real_dirname(app_args.conf_file.c_str());
		if(chdir(conf_dir.c_str()) == -1){
			fprintf(stderr, "error chdir: %s\n", conf_dir.c_str());
			exit(1);
		}
	}

	app_args.pidfile = conf->get_str("pidfile");

	if(app_args.start_opt == "stop"){
		kill_process();
		exit(0);
	}
	if(app_args.start_opt == "restart"){
		if(file_exists(app_args.pidfile)){
			kill_process();
		}
	}
	
	check_pidfile();
	
	{ // logger
		std::string log_output;
		std::string log_level_;
		int64_t log_rotate_size;

		log_level_ = conf->get_str("logger.level");
		strtolower(&log_level_);
		if(log_level_.empty()){
			log_level_ = "debug";
		}
		int level = Logger::get_level(log_level_.c_str());
		log_rotate_size = conf->get_int64("logger.rotate.size");
		log_output = conf->get_str("logger.output");
		if(log_output == ""){
			log_output = "stdout";
		}
		if(log_open(log_output.c_str(), level, true, log_rotate_size) == -1){
			fprintf(stderr, "error opening log file: %s\n", log_output.c_str());
			exit(1);
		}
	}

	app_args.sync_path = conf->get_str("sync.path");
	if(app_args.sync_path.empty()){
		app_args.sync_path = ".";
	}
	if(!is_dir(app_args.sync_path.c_str())){
		fprintf(stderr, "'%s' is not a directory or not exists!\n", app_args.sync_path.c_str());
		exit(1);
	}
        app_args.init_deep = str_to_int( conf->get_str("sync.init_deep") );
        app_args.begin_deep = str_to_int( conf->get_str("sync.begin_deep") );
        app_args.end_deep = str_to_int(conf->get_str("sync.end_deep"));
	// WARN!!!
	// deamonize() MUST be called before any thread is created!
	if(app_args.is_daemon){
            daemonize();
	}
}

void Application::watch_dir_thread(threadpool< std::string >* pool, Inotify *inot, std::string sync_path){     
    int path_deep = dir_deep(sync_path.c_str());                  
    if(  path_deep == app_args.init_deep ){  //添加初始化监控目录    
        inot->watch_dir(inot->inotify_fd, sync_path.c_str(), inot->mask);        
    } 
    
    if(  path_deep >= app_args.begin_deep ){  //开始扫描的深度       
        if( !( pool->append(sync_path) ) ){ //添加到同步进程池           
            log_info("scan pool append work queue failure. [path: %s]", sync_path.c_str());
            return;
        }                                  
        return;
    } 
    
    DIR *dir;    
    dir = opendir(sync_path.c_str());
    struct dirent *ptr;    

    if(NULL != dir){
        char sub_path[MAX_PATH_LEN];        
        while( (ptr = readdir(dir)) != NULL ){
            if( strncmp(ptr->d_name, ".", 1) == 0 ){
                continue;
            }
            memset(sub_path, '\0', sizeof(sub_path));
            if( strlen(sync_path.c_str()) + 1 + strlen(ptr->d_name) + 1 > MAX_PATH_LEN ){
                log_info("sub file path is to long. %s/%s", sync_path.c_str(), ptr->d_name);
                continue;
            }            
            snprintf(sub_path, sizeof(sub_path) - 1, "%s/%s", sync_path.c_str(), ptr->d_name);
            if( is_dir(sub_path) ){                
                watch_dir_thread(pool, inot, sub_path);
            }
        }
        closedir(dir);
    }    
     
}

int Application::read_pid(){
	if(app_args.pidfile.empty()){
		return -1;
	}
	std::string s;
	file_get_contents(app_args.pidfile, &s);
	if(s.empty()){
		return -1;
	}
	return str_to_int(s);
}

void Application::write_pid(){
	if(!app_args.is_daemon){
		return;
	}
        
	if(app_args.pidfile.empty()){
		return;
	}
	int pid = (int)getpid();
        
	std::string s = str(pid);        
	int ret = file_put_contents(app_args.pidfile, s);
       
	if(ret == -1){
		log_error("Failed to write pidfile '%s'(%s)", app_args.pidfile.c_str(), strerror(errno));
		exit(1);
	}
}

void Application::check_pidfile(){
	if(!app_args.is_daemon){
		return;
	}
	if(app_args.pidfile.size()){
		if(access(app_args.pidfile.c_str(), F_OK) == 0){
			fprintf(stderr, "Fatal error!\nPidfile %s already exists!\n"
				"Kill the running process before you run this command,\n"
				"or use '-s restart' option to restart the server.\n",
				app_args.pidfile.c_str());
			exit(1);
		}
	}
}

void Application::remove_pidfile(){
	if(!app_args.is_daemon){
		return;
	}
	if(app_args.pidfile.size()){
		remove(app_args.pidfile.c_str());
	}
}

void Application::kill_process(){
	int pid = read_pid();
	if(pid == -1){
		fprintf(stderr, "could not read pidfile: %s(%s)\n", app_args.pidfile.c_str(), strerror(errno));
		exit(1);
	}
	if(kill(pid, 0) == -1 && errno == ESRCH){
		fprintf(stderr, "process: %d not running\n", pid);
		remove_pidfile();
		return;
	}
	int ret = kill(pid, SIGTERM);
	if(ret == -1){
		fprintf(stderr, "could not kill process: %d(%s)\n", pid, strerror(errno));
		exit(1);
	}
	
	while(file_exists(app_args.pidfile)){
		usleep(100 * 1000);
	}
}