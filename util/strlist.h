#ifndef __UTIL_STRLIST_H
#define __UTIL_STRLIST_H

struct link_entry;
struct link_entry{
    struct link_entry *next_ptr;
    char *str;
};

typedef struct link_entry *link_entry_t;

struct str_link;
struct str_link{
    int len;
    link_entry_t head;
    link_entry_t tail;
};

typedef struct str_link *str_link_t;

str_link_t create_strlink();
void destroy_strlink(str_link_t link);
void insert_strlink(link_entry_t str,str_link_t link);
void remove_strlink(const char *str, str_link_t link);
int find_strlink(const char *str, str_link_t link);

#endif