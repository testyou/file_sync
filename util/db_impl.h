#ifndef __UTIL_DBIMPL_H
#define __UTIL_DBIMPL_H

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <leveldb/db.h>


class DBImpl{
    public:
        DBImpl(std::string db_path);
        ~DBImpl();
        int set(std::string key, std::string val);
        int get(std::string key, std::string *val);
        int incr(std::string name, int64_t by, int64_t *new_val);               
        int scan(std::string name, std::string start, std::string end, uint64_t limit);
    public:
        leveldb::DB *db;
        leveldb::Options options;
        leveldb::Status status;
        leveldb::WriteOptions write_options;
};

#endif