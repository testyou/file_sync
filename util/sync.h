#ifndef __UTIL_SYNC_H
#define __UTIL_SYNC_H
#include <string>
#include <stdint.h>
#include <queue>
#include <map>
#include <pthread.h>
#include "config.h"

struct sync_struct;
struct sync_struct{
    char *opt;
    char *path;
};

typedef struct sync_struct *sync_t;

struct slave_info;
struct slave_info {
    int port;
    char *path;
};
typedef struct slave_info *s_info;

using namespace std;
class Config;

class Sync{   
    private:
        volatile bool thread_quit;
        pthread_t run_thread_tid;
        static void* _run_thread(void *arg);
    public:   
        int recv_fd;
        queue<sync_t> sq;
        std::map<string, s_info> host_map;
        int slave_num = 0;
    public:
        Sync();
        Sync(Config *conf, int recv_fd);
        ~Sync();
        void init();
        void stop();
        void run();
        int event_check(int fd);
        std::string get_sync_path(const char *event_path, const char *slave_path);
        void add_slave_host(string ip, s_info info);
        void handle_data(char *data);
        void handle_sync();
        void insert_syncq(char *opt, char *path);
        void create_dir(const char *dir);
        void remove(const char *path);
        void sync_dir(const char *ip);
        void update_attrib(const char *path);
        void update_file(const char *file); 
        int exec_cmdstr(const char *cmdstr);       
    public:
        Config *conf;         
        
};

#endif