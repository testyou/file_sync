#ifndef UTIL_APP_H
#define UTIL_APP_H

#include <string>
#include <string.h>
#include "threadpool.h"
#include "inotify.h"

#define MAX_PATH_LEN 1024

class Config;

class Application{
public:
	Application(){};
	virtual ~Application(){};

	int main(int argc, char **argv);
	
	virtual void usage(int argc, char **argv);
	virtual void welcome() = 0;
        void watch_dir_thread(threadpool< std::string >* pool, Inotify *inot, std::string sync_path);//virtual 后重载，编译时报错undefined reference to `Application::watch_dir_thread(threadpool<std::string>*, Inotify*, std::string)'        
	virtual void run() = 0;

protected:
	struct AppArgs{
		bool is_daemon;
		std::string pidfile;
		std::string conf_file;
		std::string sync_path;
		std::string start_opt;  
                int init_deep = 0;
                int begin_deep = 0;
                int end_deep = 0;

		AppArgs(){
			is_daemon = false;
			start_opt = "start";
		}
	};

	Config *conf;
	AppArgs app_args;
	
private:
	void parse_args(int argc, char **argv);
	void init();                    

	int read_pid();
	void write_pid();
	void check_pidfile();
	void remove_pidfile();
	void kill_process();        
};

#endif
