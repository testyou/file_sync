#ifndef __UTIL_INOTIFY_H
#define __UTIL_INOTIFY_H

#include <string>
#include <map>
#include <queue>
#include "config.h"
#include "event_queue.h"
#include "locker.h"

#define MAX_MAP_LEN 9999999

using namespace std;

class Inotify{        
    public:
        Config *conf;
        int inotify_fd;
        int send_fd;
        unsigned long mask;
        int keep_running;    
        int watched_items = 0;
        int end_deep = 0;
        std::map<int, string> watch_dir_map;
        char  *dirmap[MAX_MAP_LEN] = {0};
        queue_t q;  
    public:
        Inotify();
        Inotify(Config *conf, int send_fd);
        ~Inotify();
        int open_inotify_fd();
        void set_mask(unsigned long mask);
        void insert_watch_dir_map(int wd, const char *dirname);
        int insert_dir_map(int wd, const char *dir);
        std::string get_wd_path(int wd);
        std::string get_event_path(int wd, const char *cur_name);
        int watch_dir(int fd, const char *dirname, unsigned long mask);
        void recur_watch_dir(int fd, const char *dirname, unsigned long mask);        
        void process(std::string dir);      
        int ignore_wd(int fd, int wd);
        int event_check(int fd);        
        int read_event(int fd, struct inotify_event *event);
        int read_events(queue_t, int);
        void handle_event(queue_entry_t event);
        void handle_events(queue_t);
        int process_inotify_events(queue_t q, int fd);
        int send_data_to_pipe(const char *operate, const char *path);        
        int close_inotify_fd(int fd);
    private:
        locker watchdir_locker;
};

#endif