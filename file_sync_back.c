#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "util/config.h"
#include "util/inotify.h"
#include "util/sync.h"


int main(int argc, char** argv){    
    
    Config *conf = new Config("file_sync.conf");  
    if(conf->user != conf->group){
        printf("AAA\n");
    }
    
    printf("conf user: %s, group: %s,  allow_ip: %s len:%d\n", conf->user.c_str(), conf->group.c_str(), conf->allow_ip_list->tail->str, conf->allow_ip_list->len );
    Inotify *inot = new Inotify(conf);    
    //Sync *sync = new Sync(conf);
    pid_t pid = fork();
    assert(pid >= 0);
    if( pid > 0){
        printf("parent pid: %d. child pid = %d write pipefd: %d\n", (int)getpid(), pid, inot->conf->pipefd[0]);  
        //inot->recur_watch_dir(inot->inotify_fd, inot->conf->sync_path.c_str(), IN_ALL_EVENTS);
        //int ret = send(inot->conf->pipefd[0], "/home/xiesunway/bb/", sizeof("/home/xiesunway/bb/"), 0);
        int ret = send(inot->conf->pipefd[0], "create_dir;/home/xiesunway/bb/", strlen("create_dir;/home/xiesunway/bb/"), 0);
        printf("parent ret: %d\n", ret);
        inot->process_inotify_events(inot->q, inot->inotify_fd);        
    }else{
        //close(inot->conf->pipefd[0]);
        //while(1){
            printf("child is running.\n");
            //std::map<int, string>::iterator itr = inot->watch_dir_map.begin();
            //itr = inot->watch_dir_map.find(4);
            /*if (itr != inot->watch_dir_map.end()) {
                //std::cout << "Item:" << itr->first << " found, content: " << itr->second << std::endl;
                //printf("Item: %d found, content: %0.2f\n", itr->first, itr->second);
                printf("Item: %d found, content: %s\n", itr->first, itr->second.c_str());
            }
            for(;itr != inot->watch_dir_map.end();){
                printf("Item: %d , content: %s\n", itr->first, itr->second.c_str());
                ++itr;
            }
            */
            char buf[1024];
            memset(buf, '\0', sizeof(buf));
            int ret;
            int fd = inot->conf->pipefd[1];
            //fd_set rfds;
            //FD_ZERO(&rfds);
            //FD_SET(fd, &rfds);
            //int sfd =  select(FD_SETSIZE, &rfds, NULL, NULL, NULL);
            //while(sfd){
                ret = recv(fd, buf, sizeof(buf), 0);
                //if(ret == 0){
                //   break;
                //}
                if(ret < 0){
                    sleep(3);
                    //continue;
                }
            //}
            printf("ret: %d, buf:%s\n", ret, buf);            
            
            //Sync *_sync = sync;
            pid_t pid = fork();
            if(pid == 0){                
                //sync->handle_data(buf);
                //execl("/usr/bin/ssh", "ssh -p 11022 ", m_buf, 0);    
                //char exec_buf[200];
                //memset(exec_buf, '\0', sizeof(exec_buf));
                std::string scp_buf = "xiesunway@221.234.43.222:";
                scp_buf += buf;
                printf("ret: %d, buf:%s, scp_buf:%s\n", ret, buf, scp_buf.c_str());
                //execl("/usr/bin/scp", "scp", "-P", "11022", exec_buf.c_str(), "/home/xiesunway", NULL);  
                
                //std::string rm_buf = "xiesunway@221.234.43.222";
                //printf("ret: %d, buf:%s, rm_buf:%s\n", ret, buf, rm_buf.c_str());
                //execl("/usr/bin/ssh", "ssh", "-p", "11022", rm_buf.c_str(), "rm", "-rf", buf, NULL);
                
                //std::string rsync_buf = "xiesunway@221.234.43.222:";//rsync -v -u -a --delete --rsh=ssh --stats localfile.txt username@remote_ip:/home/username/
                //rsync_buf += buf;
                //printf("ret: %d, buf:%s, rsync_buf:%s\n", ret, buf, rsync_buf.c_str());
                //execl("/usr/bin/rsync", "rsync", "-v", "-u", "-a", "--dirs", "--delete", "--rsh=ssh -p 11022", "--stats", buf, rsync_buf.c_str(), NULL);
                //execl("/usr/bin/ssh", "ssh", "-p", "11022", rm_buf.c_str(), "mkdir", "-p", buf, NULL);                
            }
        //}        
    }
    delete conf; 
    delete inot;   
    //delete sync;
    return 1;
}
