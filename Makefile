ssh-key:
	ssh-keygen -q -f /root/.ssh/bytweb_id_rsa -N ''
all:
	g++ -g -Wall -c util/inotify.cpp -o util/inotify.o -std=c++11
	g++ -g -Wall -c util/sync.cpp -o util/sync.o -std=c++11
	g++ -g -Wall -c util/app.cpp -o util/app.o -std=c++11
	g++ -g -Wall util/config.o util/strlist.o util/event_queue.o util/inotify.o util/sync.o util/db_impl.o util/app.o util/log.o file_sync.c -o file_sync -I include/ -lpthread include/leveldb/libleveldb.a -std=c++11
