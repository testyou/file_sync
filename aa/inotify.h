#ifndef __UTIL_INOTIFY_H
#define __UTIL_INOTIFY_H

#include <string>
#include <map>
#include <queue>
#include "config.h"
#include "event_queue.h"

using namespace std;

class Inotify{        
    public:
        Config *conf;
        int inotify_fd;
        unsigned long mask;
        int keep_running;    
        int watched_items;
        std::map<int, string> watch_dir_map;
        queue_t q;  
    public:
        Inotify();
        Inotify(Config *conf);
        ~Inotify();
        int open_inotify_fd();
        void set_mask(unsigned long mask);
        void insert_watch_dir_map(int wd, const char *dirname);
        std::string get_wd_path(int wd);
        std::string get_event_path(int wd, const char *cur_name);
        int watch_dir(int fd, const char *dirname, unsigned long mask);
        void recur_watch_dir(int fd, const char *dirname, unsigned long mask);
        int ignore_wd(int fd, int wd);
        int event_check(int fd);        
        int read_event(int fd, struct inotify_event *event);
        int read_events(queue_t, int);
        void handle_event(queue_entry_t event);
        void handle_events(queue_t);
        int process_inotify_events(queue_t q, int fd);
        int send_data_to_pipe(const char *operate, const char *path);        
        int close_inotify_fd(int fd);
};

#endif