#ifndef __UTIL_CONFIG_H
#define __UTIL_CONFIG_H

#include <unistd.h>
#include <stdint.h>
#include <string>
#include <queue>
#include <map>
#include "strlist.h"
#define  CONFIG_MAX_LINE 4096

using namespace std;

class Config{
    public:
        bool is_daemon;
        bool is_debug;
        std::string sync_path;
        pid_t pid;
        std::string pidfile;
        std::string user;
        std::string group;        
        std::string server_ip;
        int server_port;
        str_link_t allow_ip_list;
        str_link_t deny_ip_list;  
        std::map<string, int> slave_ip; 
        int pipefd[2];
        int sig_pipefd[2];
    public:
        Config();
        Config(const char *cfg_file);
        ~Config();
    private:
        void load(const char *cfg_file);
        void set_sync_path(char *cfg_line);
        void set_debug(char *cfg_line);
        void set_pidfile(char *cfg_line);
        void set_user(char *cfg_line);
        void set_group(char *cfg_line);
        void set_server_ip(char *cfg_line);
        void set_server_port(char *cfg_line);
        void set_allow_ip(char *cfg_line);
        void set_deny_ip(char *cfg_line);
        void set_slave_ip(char *cfg_line);
};
#endif